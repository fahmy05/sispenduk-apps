import * as React from 'react';
import { Button, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Login from './src/screens/Login';
import ForgotPassScreen from './src/screens/ForgotPassScreen';
import Register from './src/screens/Register';
import Verifikasi from './src/screens/VerifikasiScreen';
import OTPScreen from './src/screens/OTPScreen';
import RingkasanScreen from './src/screens/RingkasanScreen';
import SuccessScreen from './src/screens/SuccessScreen';


const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator 
        initialRouteName="Login"
        screenOptions={{
          drawerPosition: 'right',
        }}
      >
        <Drawer.Screen name="Login" component={Login} options={({ navigation, route }) => ({
            headerShown: false,
        })}/>
        <Drawer.Screen name="ForgotPass" component={ForgotPassScreen} options={({ navigation, route }) => ({
            headerShown: false,
        })}/>
        <Drawer.Screen name="Register" component={Register} options={({ navigation, route }) => ({
            headerShown: false,
        })}/>
        <Drawer.Screen name="Verifikasi" component={Verifikasi} options={({ navigation, route }) => ({
            headerShown: false,
        })}/>
        <Drawer.Screen name="OTP" component={OTPScreen} options={({ navigation, route }) => ({
            headerShown: false,
        })}/>
        <Drawer.Screen name="Ringkasan" component={RingkasanScreen} options={({ navigation, route }) => ({
            headerShown: false,
        })}/>
        <Drawer.Screen name="Success" component={SuccessScreen} options={({ navigation, route }) => ({
            headerShown: false,
        })}/>
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
