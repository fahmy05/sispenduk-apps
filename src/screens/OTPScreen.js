import React, { useState,useEffect,createRef } from 'react';
import { ScrollView, TouchableOpacity, Button, View, TextInput, Dimensions, StyleSheet, Text, Image, Cli} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import OTPInputView from '@twotalltotems/react-native-otp-input';
const {height,width} = Dimensions.get('screen')

export default function OTPScreen({ navigation }) {
    return (
    <ScrollView>
        <View style={{ flex:1, padding:'2%'}}>
        {/* header */}
        <View style={{ height:70, flexDirection:'row', justifyContent:'space-between' }}>
            <View style={{ alignItems:'center',justifyContent:'flex-start',flexDirection:'row'}}>
                <Image
                    style={{
                        width:45,
                        height:45,
                        margin:'2%'
                    }}
                    source={require('../../assets/IconApps.png')}
                />
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        textAlign:'center',
                        fontSize:20,
                        color:'#000'
                    }}
                >Desa Sukacinta</Text>
            </View>
            <View style={{ flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.toggleDrawer()
                    }}
                >
                    <Icon name="bars" size={25} backgroundColor="#3b5998"/>
                </TouchableOpacity>
            </View>
        </View>
        {/* header */}

        {/* contain */}
        <View style={{
            // backgroundColor:'red',
            marginLeft:'3%',
            marginRight:'3%',
            borderRadius:10,
            backgroundColor:'rgba(183, 183, 183, 0.07)',
            // height:'80%',
            // width:'94%',
            flexDirection:'column',
            alignItems:'center'
        }}>
            <Image
                    style={{
                        resizeMode:'contain',
                        // width:'80%',
                        // height:'50%'
                        // margin:'3%'
                    }}
                    source={require('../../assets/OTPScreen.png')}
            />
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >Selamat Datang di Website</Text>
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >RW 001 Desa Sukacinta</Text>

            <Text
                style={{
                    marginTop:'2%',
                    textAlign:'center',
                    fontSize:15,
                    fontFamily:'Poppins-Bold'
                }}
            >Verifikasi</Text>
            
            <View
                style={{
                    width:'90%',
                    flexDirection:'column',
                    justifyContent:'center'
                }}
            >
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        fontSize:14
                    }}
                >
                    Masukkan OTP
                </Text>
            </View>

            <View
                style={{
                    width:'90%',
                    flexDirection:'column',
                    justifyContent:'center'
                }}
            >
                <Text
                    style={{
                        fontFamily:'Poppins-Italic',
                        fontSize:12
                    }}
                >
                    Masukkan 6 Digit kode verifikasi yang telah dikirimkan via no. handphone
                </Text>
            </View>
            <OTPInputView
                style={{width: '80%', height: 80, color:'#333333'}}
                pinCount={6}
                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                // onCodeChanged = {code => { this.setState({code})}}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled = {(code) => {
                    console.log(`Code is ${code}, you are good to go!`)
                }}
            />
            <View
                style={{
                    width:'90%',
                    flexDirection:'row',
                }}
            >
                <Text
                    style={{
                        fontFamily:'Poppins-Italic',
                        fontSize:12,
                        marginRight:'2%'
                    }}
                >
                    Belum Menerima Kode ?
                </Text>
                <Text
                    style={{
                        fontFamily:'Poppins-BoldItalic',
                        fontSize:12,
                        textDecorationLine:'underline'
                        
                    }}
                >
                    Kirim Ulang
                </Text>
            </View>
            <View
                style={{
                    width:'100%',
                    height:70,
                    justifyContent:'space-around',
                    flexDirection:'row',
                    alignItems:'center'
                }}
            >
                <View
                    style={{
                        width:'25%',
                        height:40,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:15,
                            color:'#fff'
                        }}
                    >Simpan</Text>
                </View>
                <View
                    style={{
                        width:'25%',
                        height:40,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:15,
                            color:'#fff'
                        }}
                    >Simpan</Text>
                </View>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.navigate('Success')
                    }}
                    style={{
                        width:'25%',
                        height:40,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:15,
                            color:'#fff'
                        }}
                    >Lanjut</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#333'
    },
    borderStyleBase: {
        width: 30,
        height: 45
      },
    
      borderStyleHighLighted: {
      },
    
      underlineStyleBase: {
        width: 30,
        height: 45,
        borderRadius:8,
        color:'#333333',
        backgroundColor:'#ffffff'
      },
    
      underlineStyleHighLighted: {
        backgroundColor:'#ffffff'
      }
});
  