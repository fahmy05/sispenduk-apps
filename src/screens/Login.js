import * as React from 'react';
import { Button, View, TextInput, Dimensions, StyleSheet, Text, Image } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
const {height,width} = Dimensions.get('screen')
export default function Login({ navigation }) {
  
    return (
    <ScrollView>
        <View style={{ width,height:height*1.2, padding:'2%'}}>
        {/* header */}
        <View style={{ width:'100%', height:'10%',flexDirection:'row' }}>
            <View style={{ width:'80%',alignItems:'center', height:'100%',justifyContent:'flex-start',flexDirection:'row'}}>
                <Image
                    style={{
                        width:45,
                        height:45,
                        margin:'2%'
                    }}
                    source={require('../../assets/IconApps.png')}
                />
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        textAlign:'center',
                        fontSize:20,
                        color:'#000'
                    }}
                >Desa Sukacinta</Text>
            </View>
            <View style={{ width:'20%', height:'100%', flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.toggleDrawer()
                    }}
                >
                    <Icon name="bars" size={25} backgroundColor="#3b5998"/>
                </TouchableOpacity>
            </View>
        </View>
        {/* header */}

        {/* contain */}
        <View style={{
            // backgroundColor:'red',
            marginLeft:'3%',
            marginRight:'3%',
            borderRadius:10,
            backgroundColor:'rgba(183, 183, 183, 0.07)',
            height:'80%',
            width:'94%',
            flexDirection:'column',
            alignItems:'center'
        }}>
            <Image
                    style={{
                        // resizeMode:'contain',
                        width:'80%',
                        height:'50%'
                        // margin:'3%'
                    }}
                    source={require('../../assets/LandingScreen.png')}
            />
            <Text
                style={{
                    textAlign:'center',
                    fontSize:20,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >Hello, Welcome Back</Text>
            <View style={{
                width:'100%',
                height:'15%',
                margin:'2%',
                marginBottom:'1%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Regular',
                        fontSize:15,
                        marginLeft:'4%'
                    }}
                >NIK</Text>
                <TextInput
                    style={{
                        height:'70%',
                        width:'100%',
                        borderRadius:35,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:'15%',
                marginTop:'1%',
                margin:'2%',
                padding:'2%'

            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Regular',
                        fontSize:15,
                        marginLeft:'4%'
                    }}
                >Password</Text>
                <TextInput
                    style={{
                        height:'70%',
                        width:'100%',
                        borderRadius:35,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:'15%',
                margin:'0.5%',
                borderBottomWidth:1,
                borderBottomColor:'#000',
                padding:'2%'
            }}>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.navigate('ForgotPass')
                    }}
                >
                    <Text
                    style={{
                        color:'#3EAEFF',
                        fontFamily:'Poppins-Regular',
                        fontSize:15,
                        marginLeft:'4%'
                    }}
                >Forgot Password ?</Text>
                </TouchableOpacity>
                
                <TouchableOpacity>
                <View
                    style={{
                        height:'80%',
                        width:'100%',
                        justifyContent:'center',
                        alignItems:'center',
                        borderRadius:35,
                        backgroundColor:'#3F5794BF'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:20,
                            textAlign:'center',
                            color:'#fff'
                        }}
                    >Login</Text>
                </View>
                </TouchableOpacity>
                
            </View>
            <View style={{
                margin:'2%',
                justifyContent:'center',
                flexDirection:'row',
                height:'5%',
                width:'98%'
            }}>
                <Text
                    style={{
                        fontFamily:'Poppins-Regular',
                        color:'#040404',
                        fontSize:15
                    }}
                >Don't Have an Account Yet ? </Text>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.navigate('Register')
                    }}
                >
                    <Text
                        style={{
                            color:'#3EAEFF',
                            fontSize:15,
                            fontFamily:'Poppins-Regular'
                        }}
                    >Sign Up</Text>
                </TouchableOpacity>
                
            </View>
        </View>
    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#f0f0f0',
      justifyContent: 'center',
    }
});
  