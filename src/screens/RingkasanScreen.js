import React, { useState,useEffect,useRef } from 'react';
import { ScrollView,TouchableOpacity, Button, View, TextInput, Dimensions, StyleSheet, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { Picker } from '@react-native-picker/picker';
import DatePicker from 'react-native-date-picker';
import CheckBox from '@react-native-community/checkbox';
import Recaptcha from 'react-native-recaptcha-that-works';
const {height,width} = Dimensions.get('screen')
export default function RingkasanScreen({ navigation }) {
    const recaptcha = useRef();
    const [isSelected,setIsSelected] = useState(false)
    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)
    const [kewarganegaraan,setKewarganegaraan] = useState()
    const send = () => {
        console.log('send!');
        this.recaptcha.current.open();
    }

    const onVerify = token => {
        console.log('success!', token);
    }

    const onExpire = () => {
        console.warn('expired!');
    }
    const setDataPicker = (itemValue) => {
        // if(itemValue!==kewarganegaraan){
            setKewarganegaraan(itemValue) 
            console.log(kewarganegaraan)
        // }
        // console.log(itemValue)
        // setKewarganegaraan('')
        // setKewarganegaraan(itemValue)
        
    }
    const dateConverter = (dateInput) => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        const dateObj = dateInput;
        const month = monthNames[dateObj.getMonth()];
        const day = String(dateObj.getDate()).padStart(2, '0');
        const year = dateObj.getFullYear();
        const output = month  + '\n'+ day  + ',' + year;
        return output
    }
    useEffect(() => {    
        dateConverter(new Date())
        // recaptcha.current.open()
        setKewarganegaraan("WNI")
    },[]);
    return (
    <ScrollView>
        <View style={{ flex:1, padding:'2%'}}>
        {/* header */}
        <View style={{ height:70, flexDirection:'row', justifyContent:'space-between' }}>
            <View style={{ alignItems:'center',justifyContent:'flex-start',flexDirection:'row'}}>
                <Image
                    style={{
                        width:45,
                        height:45,
                        margin:'2%'
                    }}
                    source={require('../../assets/IconApps.png')}
                />
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        textAlign:'center',
                        fontSize:20,
                        color:'#000'
                    }}
                >Desa Sukacinta</Text>
            </View>
            <View style={{ flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.toggleDrawer()
                    }}
                >
                    <Icon name="bars" size={25} backgroundColor="#3b5998"/>
                </TouchableOpacity>
            </View>
        </View>
        {/* header */}

        {/* contain */}
        <View style={{
            // backgroundColor:'red',
            marginLeft:'3%',
            marginRight:'3%',
            borderRadius:10,
            backgroundColor:'rgba(183, 183, 183, 0.07)',
            // height:'80%',
            // width:'94%',
            flexDirection:'column',
            alignItems:'center'
        }}>
            <Image
                    style={{
                        resizeMode:'contain',
                        // width:'80%',
                        // height:'50%'
                        // margin:'3%'
                    }}
                    source={require('../../assets/Home.png')}
            />
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >Selamat Datang di Website</Text>
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >RW 001 Desa Sukacinta</Text>

            <Text
                style={{
                    marginTop:'2%',
                    textAlign:'center',
                    fontSize:15,
                    fontFamily:'Poppins-Bold'
                }}
            >Ringkasan</Text>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >NIK</Text>
                <TextInput
                    keyboardType="numeric"
                    value="00213243545"
                    editable={false}
                    placeholder="Masukan NIK"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Nama Lengkap</Text>
                <TextInput
                    placeholder="Masukan Nama Lengkap"
                    value="Budi"
                    editable={false}
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Tempat Lahir</Text>
                <TextInput
                    placeholder="Masukan Tempat Lahir"
                    value="Jakarta"
                    editable={false}
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Tanggal Lahir</Text>
                <TextInput
                    value={dateConverter(date)}
                    editable={false}
                    onPressIn={()=>{
                        setOpen(true)
                    }}
                    placeholder="Masukan Tanggal Lahir"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
                <DatePicker
                    modal
                    open={open}
                    date={date}
                    mode="date"
                    onConfirm={(date) => {
                        setOpen(false)
                        setDate(date)
                    }}
                    onCancel={() => {
                        setOpen(false)
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Jenis Kelamin</Text>
                <TextInput
                    placeholder="Masukan Jenis Kelamin"
                    editable={false}
                    value="Laki - Laki"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Agama</Text>
                <TextInput
                    value="Islam"
                    editable={false}
                    placeholder="Masukan Agama"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Kewarganegaraan</Text>
                <Picker
                    placeholder="WNI"
                    enabled={false}
                    selectedValue={kewarganegaraan}
                    onValueChange={setDataPicker}
                    mode="dropdown"
                    style={{ height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                >
                    <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                    <Picker.Item label="WNA" value="WNA" />
                    <Picker.Item label="WNI" value="WNI" />
                </Picker>
            </View>
            


            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Alamat Email</Text>
                <TextInput
                    keyboardType='email-address'
                    value="Budi@gmail.com"
                    editable={false}
                    placeholder="Masukan Alamat Email"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>

            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >No. HP</Text>
                <View
                    style={{
                        height:50,
                        width:'100%',
                        flexDirection:'row',
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                >
                    <View
                        style={{
                            width:'20%',
                            height:50,
                            backgroundColor:'#3F5794',
                            borderBottomLeftRadius:10,
                            borderTopLeftRadius:10,
                            justifyContent:'center',
                            alignItems:'center'
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:'Poppins-Regular',
                                color:'#fff'
                            }}
                        >+62</Text>
                    </View>
                    <TextInput
                        placeholder="Masukan No. HP"
                        keyboardType='number-pad'
                        value="81123204"
                        editable={false}
                        style={{
                            width:'80%',
                            fontFamily:'Poppins-Regular',
                            paddingLeft:20,
                            borderBottomRightRadius:10,
                            borderTopRightRadius:10,
                            backgroundColor:'rgba(196, 196, 196, 0.16)'
                        }}
                    />
                </View>
            </View>

            <View
                style={{
                    width:'96%',
                    marginRight:'2%',
                    marginLeft:'2%',
                    height:100,
                    flexDirection:'row'
                }}
            >
                <View
                    style={{
                        width:'10%',
                        height:'100%',
                        alignItems:'center'
                    }}
                >
                    <CheckBox
                        value={isSelected}
                        onValueChange={()=>{
                            setIsSelected(!isSelected)
                            recaptcha.current.open()
                        }}
                        // style={styles.checkbox}
                    />
                </View>
                <View
                    style={{
                        width:'90%',
                        height:'100%',
                        textAlign:'justify'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Regular',
                            textAlign:'justify',
                            fontSize:12
                        }}
                    >
                       Semua informasi dan dokumen yang saya lampir kan dalam permohonan ini adalah benar dan apabila terdapat perubahan data dalam aplikasi, saya wajib segera memberikan informasi terbaru kepada RW 001
                    </Text>
                    {/* <WebView
                        source={{ html: "<p style='text-align: justify;'>Semua informasi dan dokumen yang saya lampir kan dalam permohonan ini adalah benar dan apabila terdapat perubahan data dalam aplikasi, saya wajib segera memberikan informasi terbaru kepada RW 001</p>" }}
                    /> */}
                </View>
            </View>
            <Recaptcha
                ref={recaptcha}
                siteKey="6LejsqwZAAAAAGsmSDWH5g09dOyNoGMcanBllKPF"
                baseUrl="http://127.0.0.1"
                onVerify={onVerify}
                onExpire={onExpire}
                size="normal"
            />
            {/* <Recaptcha
                ref={recaptcha}
                lang="en"
                // headerComponent={<Button title="Close" onPress={handleClosePress} />}
                // siteKey="6LejsqwZAAAAAGsmSDWH5g09dOyNoGMcanBllKPF"
                // baseUrl="http://127.0.0.1"
                size="invisible"
                theme="light"
                onLoad={() => alert('onLoad event')}
                onClose={() => alert('onClose event')}
                onError={(err) => {
                    alert('onError event');
                    console.warn(err);
                }}
                onExpire={() => alert('onExpire event')}
                onVerify={(token) => {
                    alert('onVerify event');
                    setKey(token);
                }}
            /> */}
            <View
                style={{
                    width:'100%',
                    height:70,
                    justifyContent:'flex-end',
                    flexDirection:'row',
                }}
            >
                <View
                    style={{
                        width:'70%',
                        height:'100%',
                        flexDirection:'row',
                        justifyContent:'space-around',
                        alignItems:'center'
                    }}
                >
                    <TouchableOpacity
                        style={{
                            width:'40%',
                            height:30,
                            borderRadius:10,
                            backgroundColor:'#3F5794',
                            justifyContent:'center',
                            alignItems:'center'
                        }}
                        onPress={()=>{
                            navigation.navigate('Verifikasi')
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:'Poppins-Bold',
                                fontSize:15,
                                color:'#fff'
                            }}
                        >Simpan</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width:'40%',
                            height:30,
                            borderRadius:10,
                            backgroundColor:'#3F5794',
                            justifyContent:'center',
                            alignItems:'center'
                        }}
                        onPress={()=>{
                            navigation.navigate('Verifikasi')
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:'Poppins-Bold',
                                fontSize:15,
                                color:'#fff'
                            }}
                        >Lanjut</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#f0f0f0',
      justifyContent: 'center',
    }
});
  