import React, { useState,useEffect,createRef } from 'react';
import { ScrollView, TouchableOpacity, Button, View, TextInput, Dimensions, StyleSheet, Text, Image, Cli} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
const {height,width} = Dimensions.get('screen')

export default function SuccessScreen({ navigation }) {
    return (
    <ScrollView>
        <View style={{ flex:1, padding:'2%'}}>
        {/* header */}
        <View style={{ height:70, flexDirection:'row', justifyContent:'space-between' }}>
            <View style={{ alignItems:'center',justifyContent:'flex-start',flexDirection:'row'}}>
                <Image
                    style={{
                        width:45,
                        height:45,
                        margin:'2%'
                    }}
                    source={require('../../assets/IconApps.png')}
                />
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        textAlign:'center',
                        fontSize:20,
                        color:'#000'
                    }}
                >Desa Sukacinta</Text>
            </View>
            <View style={{ flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.toggleDrawer()
                    }}
                >
                    <Icon name="bars" size={25} backgroundColor="#3b5998"/>
                </TouchableOpacity>
            </View>
        </View>
        {/* header */}

        {/* contain */}
        <View style={{
            // backgroundColor:'red',
            marginLeft:'3%',
            marginRight:'3%',
            borderRadius:10,
            backgroundColor:'rgba(183, 183, 183, 0.07)',
            // height:'80%',
            // width:'94%',
            flexDirection:'column',
            alignItems:'center'
        }}>
            <Image
                    style={{
                        resizeMode:'contain',
                        // width:'80%',
                        // height:'50%'
                        // margin:'3%'
                    }}
                    source={require('../../assets/successRegis.png')}
            />
            

            <View
                style={{
                    width:'100%',
                    flexDirection:'column',
                    justifyContent:'center'
                }}
            >
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        fontSize:12,
                        textAlign:'center'
                    }}
                >
                    Alhamdulillah, proses registrasi akun Anda berhasil! silahkan login untuk masuk ke website RT/RW. Terima Kasih atas kerjasamanya
                </Text>
            </View>
            <View
                style={{
                    width:'100%',
                    height:70,
                    justifyContent:'flex-end',
                    flexDirection:'row',
                    alignItems:'center'
                }}
            >
                <View
                    style={{
                        width:'30%',
                        height:'100%',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <TouchableOpacity
                        onPress={()=>{
                            navigation.navigate('Login')
                        }}
                        style={{
                            width:'90%',
                            height:40,
                            borderRadius:10,
                            backgroundColor:'#3F5794',
                            justifyContent:'center',
                            alignItems:'center'
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:'Poppins-Bold',
                                fontSize:15,
                                color:'#fff'
                            }}
                        >Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#333'
    },
    borderStyleBase: {
        width: 30,
        height: 45
      },
    
      borderStyleHighLighted: {
      },
    
      underlineStyleBase: {
        width: 30,
        height: 45,
        borderRadius:8,
        color:'#333333',
        backgroundColor:'#ffffff'
      },
    
      underlineStyleHighLighted: {
        backgroundColor:'#ffffff'
      }
});
  