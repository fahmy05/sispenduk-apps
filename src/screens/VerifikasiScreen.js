import React, { useState,useEffect,createRef } from 'react';
import { ScrollView, TouchableOpacity, Button, View, TextInput, Dimensions, StyleSheet, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ActionSheet from "react-native-actions-sheet";
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
const {height,width} = Dimensions.get('screen')
const actionSheetRef = createRef();
export default function Verifikasi({ navigation }) {
    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)
    const openGallery = async () => {
        try {
            const res = await DocumentPicker.pickSingle({
                type: [DocumentPicker.types.images],
            });
            if(res.size <= 5000000){
                // let dataNPWP = {
                //     name:res.name,
                //     size:res.size,
                //     uri:res.uri,
                //     type:res.type
                // }
                // setNpwp(dataNPWP);
                // setNpwpBE(dataNPWP);
                actionSheetRef.current?.setModalVisible(false);
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                alert('Canceled from single doc picker');
            } else {
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    };
    const [kewarganegaraan,setKewarganegaraan] = useState(null)
    const setDataPicker = (itemValue) => {
        // if(itemValue!==kewarganegaraan){
            setKewarganegaraan(itemValue) 
            console.log(kewarganegaraan)
        // }
        // console.log(itemValue)
        // setKewarganegaraan('')
        // setKewarganegaraan(itemValue)
        
    }
    const dateConverter = (dateInput) => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        const dateObj = dateInput;
        const month = monthNames[dateObj.getMonth()];
        const day = String(dateObj.getDate()).padStart(2, '0');
        const year = dateObj.getFullYear();
        const output = month  + '\n'+ day  + ',' + year;
        return output
    }
    useEffect(() => {    
        dateConverter(new Date())
        setKewarganegaraan(null)
    },[]);
    return (
    <ScrollView>
        <View style={{ flex:1, padding:'2%'}}>
        {/* header */}
        <View style={{ height:70, flexDirection:'row', justifyContent:'space-between' }}>
            <View style={{ alignItems:'center',justifyContent:'flex-start',flexDirection:'row'}}>
                <Image
                    style={{
                        width:45,
                        height:45,
                        margin:'2%'
                    }}
                    source={require('../../assets/IconApps.png')}
                />
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        textAlign:'center',
                        fontSize:20,
                        color:'#000'
                    }}
                >Desa Sukacinta</Text>
            </View>
            <View style={{ flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.toggleDrawer()
                    }}
                >
                    <Icon name="bars" size={25} backgroundColor="#3b5998"/>
                </TouchableOpacity>
            </View>
        </View>
        {/* header */}

        {/* contain */}
        <View style={{
            // backgroundColor:'red',
            marginLeft:'3%',
            marginRight:'3%',
            borderRadius:10,
            backgroundColor:'rgba(183, 183, 183, 0.07)',
            // height:'80%',
            // width:'94%',
            flexDirection:'column',
            alignItems:'center'
        }}>
            <Image
                    style={{
                        resizeMode:'contain',
                        // width:'80%',
                        // height:'50%'
                        // margin:'3%'
                    }}
                    source={require('../../assets/LoginScreen.png')}
            />
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >Selamat Datang di Website</Text>
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >RW 001 Desa Sukacinta</Text>

            <Text
                style={{
                    marginTop:'2%',
                    textAlign:'center',
                    fontSize:15,
                    fontFamily:'Poppins-Bold'
                }}
            >Verifikasi</Text>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Alamat Email</Text>
                <TextInput
                    keyboardType='email-address'
                    placeholder="Masukan Alamat Email"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Password</Text>
                <TextInput
                    placeholder="Masukan Password"
                    keyboardType='visible-password'
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Ulangi Password</Text>
                <TextInput
                    placeholder="Masukan Konfirmasi Password"
                    keyboardType='visible-password'
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>

            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Upload KTP</Text>
                <View
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        justifyContent:'space-around',
                        alignItems:'center',
                        flexDirection:'row',
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                >
                    <View
                        style={{
                            width:'60%',
                            height:30,
                            alignItems:'center',
                            justifyContent:'center'
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:'Poppins-Regular',
                                fontSize:12
                            }}
                        >
                            Format .jpg, .png max size 1 MB.
                        </Text>
                    </View>
                    <TouchableOpacity
                        style={{
                            width:'30%',
                            height:30,
                            backgroundColor:'#9CC5E2',
                            borderRadius:10,
                            alignItems:'center',
                            justifyContent:'center'
                        }}
                        onPress={()=>{
                            actionSheetRef.current?.setModalVisible(true);
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:'Poppins-Bold',
                                fontSize:12,
                                color:'#fff'
                            }}
                        >
                            Pilih
                        </Text>
                    </TouchableOpacity>
                    
                    
                </View>
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >No. HP</Text>
                <View
                    style={{
                        height:50,
                        width:'100%',
                        flexDirection:'row',
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                >
                    <View
                        style={{
                            width:'20%',
                            height:50,
                            backgroundColor:'#3F5794',
                            borderBottomLeftRadius:10,
                            borderTopLeftRadius:10,
                            justifyContent:'center',
                            alignItems:'center'
                        }}
                    >
                        <Text
                            style={{
                                fontFamily:'Poppins-Regular',
                                color:'#fff'
                            }}
                        >+62</Text>
                    </View>
                    <TextInput
                        placeholder="Masukan No. HP"
                        keyboardType='number-pad'
                        style={{
                            width:'80%',
                            fontFamily:'Poppins-Regular',
                            paddingLeft:20,
                            borderBottomRightRadius:10,
                            borderTopRightRadius:10,
                            backgroundColor:'rgba(196, 196, 196, 0.16)'
                        }}
                    />
                </View>
            </View>

            <View
                style={{
                    width:'100%',
                    height:70,
                    justifyContent:'space-around',
                    flexDirection:'row',
                    alignItems:'center'
                }}
            >
                <View
                    style={{
                        width:'25%',
                        height:40,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:15,
                            color:'#fff'
                        }}
                    >Simpan</Text>
                </View>
                <View
                    style={{
                        width:'25%',
                        height:40,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:15,
                            color:'#fff'
                        }}
                    >Simpan</Text>
                </View>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.navigate('OTP')
                    }}
                    style={{
                        width:'25%',
                        height:40,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:15,
                            color:'#fff'
                        }}
                    >Lanjut</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    <ActionSheet ref={actionSheetRef} containerStyle={styles.container} closeOnTouchBackdrop={false}>
        <View
            style={{
                width:'100%',
                height:180,
                // justifyContent:'space-around',
                flexDirection:'column',
                // alignItems:'center',
                borderTopRightRadius:10,
                borderTopLeftRadius:10
            }}
        >
            <TouchableOpacity
                style={{
                    width:'100%',
                    height:60,
                    borderTopRightRadius:10,
                    borderTopLeftRadius:10,
                    borderColor:'#2196f3',
                    borderBottomWidth:1,
                    backgroundColor:'#333',
                    justifyContent:'center',
                    alignItems:'center'
                }}
                onPress={()=>{
                    ImagePicker.openCamera({
                        cropping: true
                      }).then(image => {
                        if (image.size <= 5000000) {        
                        //   let dataNPWP = {
                        //     name:image.path.substring(image.path.lastIndexOf('/') + 1),
                        //     size:image.size,
                        //     uri:image.path,
                        //     type:image.mime
                        //   }        
                        //   setNpwp(dataNPWP);
                        //   setNpwpBE(dataNPWP);
                        actionSheetRef.current?.setModalVisible(false);
                        } else {
                        //   setIsToast(true)
                        }
                      });
                }}
            >
                <Text
                    style={{
                        fontFamily:'Poppin-Regular',
                        color:'#fff',
                        fontSize:20
                    }}
                >Open Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={{
                    width:'100%',
                    height:60,
                    borderColor:'#2196f3',
                    borderBottomWidth:1,
                    backgroundColor:'#333',
                    justifyContent:'center',
                    alignItems:'center'
                }}
            >
                <Text
                    style={{
                        fontFamily:'Poppin-Regular',
                        color:'#fff',
                        fontSize:20
                    }}
                    onPress={openGallery}
                >Open Gallery</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={{
                    width:'100%',
                    height:60,
                    borderColor:'#2196f3',
                    borderBottomWidth:1,
                    backgroundColor:'#cc2d1f',
                    justifyContent:'center',
                    alignItems:'center'

                }}
                onPress={()=>{
                    actionSheetRef.current?.setModalVisible(false);
                }}
            >
                <Text
                    style={{
                        fontFamily:'Poppin-Regular',
                        color:'#fff',
                        fontSize:20
                    }}
                >Cancel</Text>                
            </TouchableOpacity>
        </View>
    </ActionSheet>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#333'
    }
});
  