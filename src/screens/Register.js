import React, { useState,useEffect } from 'react';
import { ScrollView, TouchableOpacity, Button, View, TextInput, Dimensions, StyleSheet, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { Picker } from '@react-native-picker/picker';
import DatePicker from 'react-native-date-picker';
const {height,width} = Dimensions.get('screen')
export default function Register({ navigation }) {
    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)
    const [kewarganegaraan,setKewarganegaraan] = useState(null)
    const setDataPicker = (itemValue) => {
        // if(itemValue!==kewarganegaraan){
            setKewarganegaraan(itemValue) 
            console.log(kewarganegaraan)
        // }
        // console.log(itemValue)
        // setKewarganegaraan('')
        // setKewarganegaraan(itemValue)
        
    }
    const dateConverter = (dateInput) => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        const dateObj = dateInput;
        const month = monthNames[dateObj.getMonth()];
        const day = String(dateObj.getDate()).padStart(2, '0');
        const year = dateObj.getFullYear();
        const output = month  + '\n'+ day  + ',' + year;
        return output
    }
    useEffect(() => {    
        dateConverter(new Date())
        setKewarganegaraan(null)
    },[]);
    return (
    <ScrollView>
        <View style={{ flex:1, padding:'2%'}}>
        {/* header */}
        <View style={{ height:70, flexDirection:'row', justifyContent:'space-between' }}>
            <View style={{ alignItems:'center',justifyContent:'flex-start',flexDirection:'row'}}>
                <Image
                    style={{
                        width:45,
                        height:45,
                        margin:'2%'
                    }}
                    source={require('../../assets/IconApps.png')}
                />
                <Text
                    style={{
                        fontFamily:'Poppins-Bold',
                        textAlign:'center',
                        fontSize:20,
                        color:'#000'
                    }}
                >Desa Sukacinta</Text>
            </View>
            <View style={{ flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity
                    onPress={()=>{
                        navigation.toggleDrawer()
                    }}
                >
                    <Icon name="bars" size={25} backgroundColor="#3b5998"/>
                </TouchableOpacity>
            </View>
        </View>
        {/* header */}

        {/* contain */}
        <View style={{
            // backgroundColor:'red',
            marginLeft:'3%',
            marginRight:'3%',
            borderRadius:10,
            backgroundColor:'rgba(183, 183, 183, 0.07)',
            // height:'80%',
            // width:'94%',
            flexDirection:'column',
            alignItems:'center'
        }}>
            <Image
                    style={{
                        resizeMode:'contain',
                        // width:'80%',
                        // height:'50%'
                        // margin:'3%'
                    }}
                    source={require('../../assets/FillingSystem.png')}
            />
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >Selamat Datang di Website</Text>
            <Text
                style={{
                    textAlign:'center',
                    fontSize:15,
                    fontWeight:'500',
                    fontFamily:'Poppins-Regular'
                }}
            >RW 001 Desa Sukacinta</Text>

            <Text
                style={{
                    marginTop:'2%',
                    textAlign:'center',
                    fontSize:15,
                    fontFamily:'Poppins-Bold'
                }}
            >Register</Text>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >NIK</Text>
                <TextInput
                    keyboardType="numeric"
                    placeholder="Masukan NIK"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Nama Lengkap</Text>
                <TextInput
                    placeholder="Masukan Nama Lengkap"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Tempat Lahir</Text>
                <TextInput
                    placeholder="Masukan Tempat Lahir"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Tanggal Lahir</Text>
                <TextInput
                    value={dateConverter(date)}
                    onPressIn={()=>{
                        setOpen(true)
                    }}
                    placeholder="Masukan Tanggal Lahir"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
                <DatePicker
                    modal
                    open={open}
                    date={date}
                    mode="date"
                    onConfirm={(date) => {
                        setOpen(false)
                        setDate(date)
                    }}
                    onCancel={() => {
                        setOpen(false)
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Jenis Kelamin</Text>
                <TextInput
                    placeholder="Masukan Jenis Kelamin"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Agama</Text>
                <TextInput
                    placeholder="Masukan Agama"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Kewarganegaraan</Text>
                <Picker
                    placeholder="Pilih"
                    selectedValue={kewarganegaraan}
                    onValueChange={setDataPicker}
                    mode="dropdown"
                    style={{ height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                >
                    <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                    <Picker.Item label="WNA" value="WNA" />
                    <Picker.Item label="WNI" value="WNI" />
                </Picker>
            </View>
            <View style={{
                width:'100%',
                height:50,
                justifyContent:'center',
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Alamat Tempat Tinggal</Text>
            </View>
            <View style={{
                width:'100%',
                height:100,
                justifyContent:'space-between',
                margin:'2%',
                flexDirection:'row',
                alignItems:'center'
            }}>
                <View style={{
                    width:'50%',
                    height:100
                }}>
                    <Text
                        style={{
                            color:'rgba(0, 0, 0, 0.32)',
                            fontFamily:'Poppins-BoldItalic',
                            fontSize:15
                        }}
                    >Provinsi</Text>
                    <Picker
                        placeholder="Pilih"
                        selectedValue={kewarganegaraan}
                        onValueChange={setDataPicker}
                        mode="dropdown"
                        style={{ height:50,
                            fontFamily:'Poppins-Regular',
                            width:'90%',
                            paddingLeft:20,
                            borderRadius:20,
                            backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                    >
                        <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                        <Picker.Item label="WNA" value="WNA" />
                        <Picker.Item label="WNI" value="WNI" />
                    </Picker>
                </View>
                <View style={{
                    width:'50%',
                    height:100
                }}>
                    <Text
                        style={{
                            color:'rgba(0, 0, 0, 0.32)',
                            fontFamily:'Poppins-BoldItalic',
                            fontSize:15
                        }}
                    >Kota / Kabupaten</Text>
                    <Picker
                        placeholder="Pilih"
                        selectedValue={kewarganegaraan}
                        onValueChange={setDataPicker}
                        mode="dropdown"
                        style={{ height:50,
                            fontFamily:'Poppins-Regular',
                            width:'90%',
                            paddingLeft:20,
                            borderRadius:20,
                            backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                    >
                        <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                        <Picker.Item label="WNA" value="WNA" />
                        <Picker.Item label="WNI" value="WNI" />
                    </Picker>
                </View>
            </View>
            <View style={{
                width:'100%',
                height:100,
                justifyContent:'space-between',
                margin:'2%',
                flexDirection:'row',
                alignItems:'center'
            }}>
                <View style={{
                    width:'50%',
                    height:100
                }}>
                    <Text
                        style={{
                            color:'rgba(0, 0, 0, 0.32)',
                            fontFamily:'Poppins-BoldItalic',
                            fontSize:15
                        }}
                    >Kecamatan</Text>
                    <Picker
                        placeholder="Pilih"
                        selectedValue={kewarganegaraan}
                        onValueChange={setDataPicker}
                        mode="dropdown"
                        style={{ height:50,
                            fontFamily:'Poppins-Regular',
                            width:'90%',
                            paddingLeft:20,
                            borderRadius:20,
                            backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                    >
                        <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                        <Picker.Item label="WNA" value="WNA" />
                        <Picker.Item label="WNI" value="WNI" />
                    </Picker>
                </View>
                <View style={{
                    width:'50%',
                    height:100
                }}>
                    <Text
                        style={{
                            color:'rgba(0, 0, 0, 0.32)',
                            fontFamily:'Poppins-BoldItalic',
                            fontSize:15
                        }}
                    >Kelurahan</Text>
                    <Picker
                        placeholder="Pilih"
                        selectedValue={kewarganegaraan}
                        onValueChange={setDataPicker}
                        mode="dropdown"
                        style={{ height:50,
                            fontFamily:'Poppins-Regular',
                            width:'90%',
                            paddingLeft:20,
                            borderRadius:20,
                            backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                    >
                        <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                        <Picker.Item label="WNA" value="WNA" />
                        <Picker.Item label="WNI" value="WNI" />
                    </Picker>
                </View>
            </View>
            <View style={{
                width:'100%',
                height:100,
                justifyContent:'space-between',
                margin:'2%',
                flexDirection:'row',
                alignItems:'center'
            }}>
                <View style={{
                    width:'50%',
                    height:100
                }}>
                    <Text
                        style={{
                            color:'rgba(0, 0, 0, 0.32)',
                            fontFamily:'Poppins-BoldItalic',
                            fontSize:15
                        }}
                    >Kode Pos</Text>
                    <Picker
                        placeholder="Pilih"
                        selectedValue={kewarganegaraan}
                        onValueChange={setDataPicker}
                        mode="dropdown"
                        style={{ height:50,
                            fontFamily:'Poppins-Regular',
                            width:'90%',
                            paddingLeft:20,
                            borderRadius:20,
                            backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                    >
                        <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                        <Picker.Item label="WNA" value="WNA" />
                        <Picker.Item label="WNI" value="WNI" />
                    </Picker>
                </View>
                <View style={{
                    width:'50%',
                    height:100,
                    flexDirection:'row',
                    justifyContent:'space-between',
                }}>
                    <View style={{
                        width:'50%',
                        height:100
                    }}>
                        <Text
                            style={{
                                color:'rgba(0, 0, 0, 0.32)',
                                fontFamily:'Poppins-BoldItalic',
                                fontSize:15
                            }}
                        >RT</Text>
                        <Picker
                            placeholder="Pilih"
                            selectedValue={kewarganegaraan}
                            onValueChange={setDataPicker}
                            mode="dropdown"
                            style={{ height:50,
                                fontFamily:'Poppins-Regular',
                                width:'90%',
                                paddingLeft:20,
                                borderRadius:20,
                                backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                        >
                            <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                            <Picker.Item label="WNA" value="WNA" />
                            <Picker.Item label="WNI" value="WNI" />
                        </Picker>
                    </View>
                    <View style={{
                        width:'50%',
                        height:100
                    }}>
                        <Text
                            style={{
                                color:'rgba(0, 0, 0, 0.32)',
                                fontFamily:'Poppins-BoldItalic',
                                fontSize:15
                            }}
                        >RW</Text>
                        <Picker
                            placeholder="Pilih"
                            selectedValue={kewarganegaraan}
                            onValueChange={setDataPicker}
                            mode="dropdown"
                            style={{ height:50,
                                fontFamily:'Poppins-Regular',
                                width:'90%',
                                paddingLeft:20,
                                borderRadius:20,
                                backgroundColor:'rgba(196, 196, 196, 0.16)' }}
                        >
                            <Picker.Item label="Pilih Kewarganegaraan" value={null} />
                            <Picker.Item label="WNA" value="WNA" />
                            <Picker.Item label="WNI" value="WNI" />
                        </Picker>
                    </View>
                </View>
            </View>


            {/* Nama Jalan */}
            <View style={{
                width:'100%',
                height:100,
                margin:'2%',
                padding:'2%'
            }}>
                <Text
                    style={{
                        color:'rgba(0, 0, 0, 0.32)',
                        fontFamily:'Poppins-Bold',
                        fontSize:15
                    }}
                >Nama Jalan</Text>
                <TextInput
                    placeholder="Masukan Nama Jalan"
                    style={{
                        height:50,
                        fontFamily:'Poppins-Regular',
                        width:'100%',
                        paddingLeft:20,
                        borderRadius:10,
                        backgroundColor:'rgba(196, 196, 196, 0.16)'
                    }}
                />
            </View>

            <View
                style={{
                    width:'100%',
                    height:70,
                    justifyContent:'space-around',
                    flexDirection:'row',
                    alignItems:'center'
                }}
            >
                <TouchableOpacity
                    style={{
                        width:'40%',
                        height:50,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                    onPress={()=>{
                        navigation.navigate('Verifikasi')
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:20,
                            color:'#fff'
                        }}
                    >Simpan</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        width:'40%',
                        height:50,
                        borderRadius:10,
                        backgroundColor:'#3F5794',
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                    onPress={()=>{
                        navigation.navigate('Verifikasi')
                    }}
                >
                    <Text
                        style={{
                            fontFamily:'Poppins-Bold',
                            fontSize:20,
                            color:'#fff'
                        }}
                    >Lanjut</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#f0f0f0',
      justifyContent: 'center',
    }
});
  